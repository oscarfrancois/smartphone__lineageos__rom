# L'opensource sur mobile/smartphone avec Lineageos

L'utilisation d'une ROM alternative de type logiciel libre offre de nombreux avantages à l'utilisateur.

On peut citer par exemple:

 * avoir un système respectant la vie privée de l'utilisateur,
 * avoir un système ouvert et réellement transparent sur son fonctionnement,
 * avoir un système qu'on peut totalement personnaliser,
 * avoir un système nettement plus rapide,
   (en enlevant par exemple les surcouches opérateur / constructeur),
 * augmenter fortement la durée de vie d'un téléphone/smartphone et ainsi diminuer la pollution,
 * augmenter l'autonomie de la batterie en ayant moins de services en tâche de fond.

Il convient aussi de relever certaines faiblesses associées à une ROM alternative:

 * risque d'endommager / "bricker" son téléphone,
 * perte de garantie,

# Les évolutions dans le monde des smartphones

De nouveaux systèmes d'exploitation mobiles complets sont en élaboration.
On peut citer par exemple:

 * [Fuschia](https://en.wikipedia.org/wiki/Google_Fuchsia) (Google)
 * [Harmony OS](https://en.wikipedia.org/wiki/Harmony_OS) (Huawei)

# Anatomie et l'écosystème Android

* [architecture générale](https://source.android.com/devices/architecture/)
* [dépôts git permettant de générer un système Android](https://android-review.googlesource.com/q/status:open)

# L'écosystème des ROM alternatives

* [LineageOS](https://lineageos.org/)
* [/e/](http://e.foundation/)

Les magasins d'application / market

* [Ajout du playstore sur lineageos](https://wiki.lineageos.org/gapps.html)
* [F-droid](https://f-droid.org/)

# Le modèle de mobile / smartphone utilisé

Nous effectuerons le flashage sur un système moyennement ancien, un Samsung S4.
Ce modèle avait été initialement lancé en 2013.

Les détails techniques de ce modèle sont accessibles par exemple via le site 
[Gsmarena](https://www.gsmarena.com/samsung_i9505_galaxy_s4-5371.php)


# Etapes à suivre

## Généralités sur le bootloader

Un premier système d'exploitation dénommé le "bootloader" est démarré à l'allumage.
C'est lui qui est en charge de lancer le système d'exploitation principal.
Il sert aussi à effectuer différents contrôles et opérations de maintenance.

Selon le mode de boot sélectionné, le bootloader chargera:
- la ROM principale
- la ROM de recovery
- un mode de flashage

## Récupération des firmwares

Les firmwares sont spécifiques à chaque modèle.

Commencez par déterminer le modèle exact de ce samsung S4 en allant dans les paramètres d'Android.

Allez ensuite sur: https://download.lineageos.org

et téléchargez les firmware correspondant à votre modèle.

chaque page de téléchargement comporte un lien vers les instructions détailées d'installation.
Prenez-en connaissance puis appliquez la procédure.

